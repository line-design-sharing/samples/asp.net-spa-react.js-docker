FROM mcr.microsoft.com/dotnet/sdk:6.0.202 AS dotnet-build
RUN apt-get -qq update && apt-get -qqy --no-install-recommends install wget gnupg git unzip
RUN curl -sL https://deb.nodesource.com/setup_17.x | bash -
RUN apt-get install -y nodejs
RUN curl -L https://npmjs.org/install.sh | bash -
WORKDIR /app
COPY src ./
RUN dotnet restore Line.Samples.NetSpa.Reactjs.csproj
RUN dotnet build Line.Samples.NetSpa.Reactjs.csproj -c Release -o /app/build

FROM node AS node-build
WORKDIR /node
COPY ./src/ClientApp ./
RUN npm install
RUN npm run build

FROM dotnet-build AS dotnet-publish
RUN dotnet publish Line.Samples.NetSpa.Reactjs.csproj -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:6.0.4
WORKDIR /app
RUN mkdir /app/wwwroot
COPY --from=dotnet-publish /app/publish .
COPY --from=node-build /node/build ./wwwroot
ENTRYPOINT ["dotnet", "Line.Samples.NetSpa.Reactjs.dll"]