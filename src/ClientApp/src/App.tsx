import React, { Component } from 'react';
import { Routes, Route } from 'react-router';
import Scenes from './scenes';
import { FetchData } from './components/FetchData';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render = () =>
    <Scenes.Layout>
      <Routes>
        <Route path='/' element={<Scenes.Home />} />
        <Route path='/test' element={<Scenes.Test />} />
        <Route path='/test/:id' element={<Scenes.Test />} />
        <Route path='/fetch-data' element={<FetchData />} />
      </Routes>
    </Scenes.Layout>
}
