import Layout from './layout';
import Home from './home';
import Test from './test';

const scenes = {
    Layout,
    Home,
    Test
}

export default scenes