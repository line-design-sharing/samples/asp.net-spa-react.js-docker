import React from 'react';

interface HomeProps { }

interface HomeComponent extends React.FunctionComponent<HomeProps> { }

const Home: HomeComponent = () => {

    return <div>
        home
    </div>
}

export default Home