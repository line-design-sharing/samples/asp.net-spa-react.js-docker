import React from 'react';
import { useParams, useSearchParams } from 'react-router-dom';

interface TestParams {
    id: string
}

interface TestProps { }

interface TestComponent extends React.FunctionComponent<TestProps> { }

const Test: TestComponent = props => {

    const params = useParams<keyof TestParams>()
    const [searchParams,] = useSearchParams()

    console.log(searchParams.keys().next())

    return <div>
        test
    </div>
}

export default Test