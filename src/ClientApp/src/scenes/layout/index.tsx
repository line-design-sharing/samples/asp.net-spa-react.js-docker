import React from 'react';
import { Link } from 'react-router-dom';

interface LayoutProps { }

interface LayoutComponent extends React.FunctionComponent<LayoutProps> { }

const Layout: LayoutComponent = props => {

    return <div>
        layout

        <ul>
            <li>
                <Link to='/'>Home</Link>
            </li>
            <li>
                <Link to='/test'>Test</Link>
            </li>
            <li>
                <Link to='/fetch-data'>Fetch data</Link>
            </li>
        </ul>
        
        {props.children}
    </div>
}

export default Layout